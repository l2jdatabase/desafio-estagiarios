<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $this->getCRMInfos("title"); ?></title>

<!-- Awesome -->
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/css/font-awesome.min.css">
<!-- Bootstramp -->
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/css/bootstrap.min.css">
<!-- Style Custom -->
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/css/style.css">

<?php if ($nameController == "HomeController" && $nameFunction == "search")
{
?>
    <style type="text/css">
        body {
            background: #F1F1EF url("<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/imgs/Background.png") no-repeat right top fixed;
            background-size: cover;
        }
    </style>
<?php } ?>