<footer>
    <div class="credits">Projetado por: Fulano de Tal - 31/12/2020</div>

    <!-- JQuery -->
    <script src="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/js/bootstrap.bundle.min.js"></script>
    <!-- Js Custom -->
    <script src="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/js/custom.js"></script>
</footer>