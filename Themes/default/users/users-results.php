<div class="row">
    <div class="title">
        <h4>Exibindo (<i><?php echo $totalCount[0]; ?></i> / <i><?php echo $totalCount[1]; ?></i>) resultados para: <i><?php echo $search; ?></i></h4>
    </div>

    <?php if (isset($usersInfos) && !empty($usersInfos))
    {
        foreach ($usersInfos["items"] as $user)
        {
    ?>
            <div class="col-sm-3"><?php $this->loadView("users/user-card", $user); ?></div>
        <?php
        }
    }
    else
    { ?>
        <?php $this->loadView("users/users-results-empty", $viewData); ?>
    <?php } ?>
</div>