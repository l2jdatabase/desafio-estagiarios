<!-- Modal -->
<div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row modal-user">
                        <div class="col-md-4">
                            <?php
                            $avatar = BASE_URL . "/Assets/Themes/" . $this->getCRMInfos("theme", false) . "/imgs/avatar_full.png";
                            ?>
                            <img src="<?php echo $avatar; ?>" class="card-img-top" alt="...">
                        </div>
                        <div class="col-md-8">
                            <h4 class="title">Renan Pecanha</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p>Username:
                                        <span class="left">MilenaMelo</span>
                                    </p>

                                    <p>Cadastrado(a):
                                        <span class="left">31/12/2020</span>
                                    </p>
                                </div>

                                <div class="col-md-6 right">
                                    <p>Seguindo:
                                        <span class="right">300</span>
                                    </p>

                                    <p>Seguidores:
                                        <span class="right">5000</span>
                                    </p>
                                </div>

                                <div class="col-md-12">
                                    <p>URL:
                                        <span class="left">https://hithub.com/MilenaMelo</span>
                                    </p>
                                </div>

                                <div class="col-md-12 right modal-user-btn">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">FECHAR</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>