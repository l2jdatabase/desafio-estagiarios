<div class="card">
    <?php
    $avatar = BASE_URL . "/Assets/Themes/" . $this->getCRMInfos("theme", false) . "/imgs/Avatar.png";
    if (isset($viewData["avatar_url"]) && !empty($viewData["avatar_url"]))
    {
        $avatar = $viewData["avatar_url"];
    }
    ?>
    <img src="<?php echo $avatar; ?>" class="card-img-top" alt="...">

    <div class="card-body">
        <h5 class="card-title"><?php echo $viewData["login"]; ?></h5>

        <p class="card-text">
            <a href="<?php echo $viewData["html_url"]; ?>" target="_blank"><?php echo $viewData["html_url"]; ?>
            </a>
            <span>Score: <?php echo number_format($viewData["score"], 2, '.', ''); ?></span>
        </p>

        <a href="javascript:;" onclick="viewModal('<?php echo $viewData['login']; ?>', '<?php echo BASE_URL; ?>')">
            <button type="button" class="btn btn-primary">Ver Mais</button>
        </a>
    </div>
</div>

<?php $this->loadView("users/user-card-modal", $viewData); ?>