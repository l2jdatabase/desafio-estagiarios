<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->loadView("head", $viewData); ?>
</head>

<body>
    <?php $this->loadView("top", $viewData); ?>
    <div class="container">
        <?php $this->loadViewInTemplate($viewName, $viewData); ?>
    </div>
    <?php $this->loadView("footer", $viewData); ?>
</body>

</html>