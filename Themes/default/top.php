<header>
    <div class="top-container">
        <div class="logotype">
            <a href="<?php echo BASE_URL; ?>">
                <img src="<?php echo BASE_URL; ?>/Assets/Themes/<?php $this->getCRMInfos("theme"); ?>/imgs/Logotype.png">
            </a>
        </div>

        <form method="POST" action="<?php echo BASE_URL; ?>/search">
            <div class="input-group mb-3 search-box">
                <?php
                $search_string = null;
                if (isset($search) && !empty($search))
                {
                    $search_string =  $search;
                } ?>
                <input type="text" class="form-control search" placeholder="Pesquisar" name="search" id="search" value="<?php echo $search_string; ?>">
                <button class=" btn btn-outline-secondary btn-search" type="input" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </form>
    </div>
</header>