function viewModal(login, base_url) {

    $.ajax({
        url: base_url + '/userinfo/' + login,
        type: 'POST',
        data: { login: login },
        beforeSend: function () {
            $('#staticBackdrop').find('.modal-body').html('Carregando...');
            $('#staticBackdrop').modal('show');
        },
        success: function (html) {
            $('#staticBackdrop').find('.modal-body').html(html);
            $('#staticBackdrop').modal('show');
        }
    });

}