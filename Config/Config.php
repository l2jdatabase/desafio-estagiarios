<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

//------------------------------------------
// REQUISIÇÕES (NÃO MEXER)
//------------------------------------------
require_once 'Debug.php';
//------------------------------------------
// VARIAVEIS (NÃO MEXER)
//------------------------------------------
global $c, $api, $mail;
$c = array();
//------------------------------------------
// INSTANCIAS (NÃO MEXER)
//------------------------------------------
$instance["mysql"]  = new Mysql();
$instance["url"]    = new Url();
//--------------------------
// CONEXÃO LOCAL
//--------------------------
$c['name'] = 'name';
$c['host'] = 'localhost';
$c['port'] = '3357';
$c['user'] = 'root';
$c['pass'] = '';
//--------------------------
// API = abC123!
//--------------------------  
$api["jwt_secret_key"] = "p3s4A!s@3lRs#4Kl57#sJkU56aKD3l@f#sZ12sWl6@#hz6U5";
//------------------------------------------
// RAIZ DO SITE
// Caso o site esteja /Raiz do site
// Padrão: "" (Vazio)
//------------------------------------------
$subpasta = "";
$instance["url"]->toDefine($subpasta);
define("FILES_FOLDER", "Assets/Uploads");
//--------------------------
// MAIL INFOS
//--------------------------
$mail["mail_name"]          = 'Support L2Baylor';
$mail["mail_username"]      = 'developer@l2jdatabase.com';
$mail["mail_password"]      = 'password_l2jdatabase';
$mail["mail_Host"]          = 'smtp.dominio.com';
$mail["mail_Port"]          = 587;
$mail["mail_SMTPAuth"]      = true;
$mail["mail_SMTPSecure"]    = false;
$mail["mail_SMTPAutoTLS"]   = false;
//------------------------------------------
// LOCALIDADE / IDIOMA SERVIDOR PACHE
//------------------------------------------
date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
ini_set('default_charset', 'utf8');
//------------------------------------------
// EXIBIR MENSAGENS DE ERROS
// 0 = SIM / 1 = NÃO
// PADRÃO: 0
//------------------------------------------
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//----------------------------------------------------------------------------------------------
// CONEXAO 01 COM BANCO DE DADOS
//----------------------------------------------------------------------------------------------
//$instance["mysql"]->connect(0, $c['name'], $c['host'], $c['port'], $c['user'], $c['pass']);
