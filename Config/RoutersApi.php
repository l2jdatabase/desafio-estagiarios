<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

global $routesApi;

$routesApi = array();
/**
 * EXEMPLES
 * $routesApi["accounts/new"]         = ['accounts/new', 'POST'];
 * $routesApi["accounts/get"]         = ['accounts/new', 'GET'];
 * 
 * Se não for um arrau ou valor não tiver segundo paramentro
 * Padrao: POST
 * $routesApi["tickets/new"]          = 'tickets/new'; 
 */

$routesApi["accounts/login"]        = 'accounts/login';
$routesApi["accounts/new"]          = 'accounts/new';
$routesApi["accounts/get"]          = 'accounts/get';
$routesApi["accounts/put"]          = 'accounts/set';
$routesApi["accounts/put/password"] = 'accounts/set_password';
$routesApi["accounts/put/donate"]   = 'accounts/set_donate';
//-----------------------------------------------------------------------
$routesApi["actions/new"]          = 'actions/new';
//-----------------------------------------------------------------------
$routesApi["server/infos"]         = 'servers/infos';
//-----------------------------------------------------------------------
$routesApi["tickets/new"]          = 'tickets/new';
$routesApi["tickets/new/comment"]  = 'tickets/new_comment';
$routesApi["tickets/get"]          = 'tickets/get';
$routesApi["tickets/get/{id}"]     = 'tickets/get/:id';
$routesApi["tickets/put"]          = 'tickets/set';
$routesApi["tickets/put/comment"]  = 'tickets/set_comment';

