<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

namespace Apps;

use Apps\ApiApp;
use Core\Applicator;

class GithubApp extends Applicator
{
    private $ApiApp;

    public function __construct()
    {
        $this->ApiApp = new ApiApp();
    }

    public function getUsers($string, $searchUserInfo = False)
    {
        $url = "https://api.github.com/search/users";

        $header = array(
            "Accept: application/vnd.github.v3+json",
            "Content-Type: text/plain",
            "User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 YaBrowser/16.3.0.7146 Yowser/2.5 Safari/537.36"
        );

        $data = $this->ApiApp->pull("GET", $url, $header, array("q" => $string));

        if (($searchUserInfo) && (isset($data["items"])) && (!empty($data["items"])) && (is_array($data["items"])))
        {
            $i = 0;
            foreach ($data["items"] as $item)
            {
                $data["items"][$i]["userInfos"] = $this->getUser($item["login"]);
                $i++;
            }
        }

        return $data;
    }

    public function getUser($login)
    {
        $url = "https://api.github.com/users/" . $login;

        $header = array(
            "Accept: application/vnd.github.v3+json",
            "Content-Type: text/plain",
            "User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 YaBrowser/16.3.0.7146 Yowser/2.5 Safari/537.36"
        );

        return $this->ApiApp->pull("GET", $url, $header);
    }
}
