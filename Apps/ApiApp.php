<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

namespace Apps;

use Core\Applicator;

class ApiApp extends Applicator
{
    public function pull($Method, $url, $header = null, $array = array(), $jsonDecode = True)
    {
        //---------------------------------------
        // Method
        //---------------------------------------
        $curl = curl_init($url);
        switch ($Method)
        {
            case "POST":
                {
                    curl_setopt($curl, CURLOPT_POST, 1);
                    if ($array)
                    {
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $array);
                    }
                    break;
                }
            case "PUT":
                {
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                    if ($array)
                    {
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $array);
                    }
                    break;
                }
            default:
                {
                    if ($array)
                    {
                        $url = sprintf("%s?%s", $url, http_build_query($array));
                    }
                }
        }
        //---------------------------------------
        // HEADER
        //---------------------------------------
        if (!empty($header))
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        //---------------------------------------
        // OPTIONS:
        //---------------------------------------
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $data = curl_exec($curl);
        curl_close($curl);
        //---------------------------------------
        // 
        //---------------------------------------
        if ($jsonDecode)
        {
            $data = json_decode($data, true);
        }
        return $data;
    }
}
