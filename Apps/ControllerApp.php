<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

namespace Apps;

use Core\Applicator;

class ControllerApp extends Applicator
{
    public function url($nameController, $nameFunction = null)
    {
        $remove = array("Controllers\\","Controller");
        $nameController = str_replace($remove, "", $nameController);  
        $nameController = strtolower($nameController);              
        $url = BASE_URL.$nameController."/";

        if(!empty($nameFunction))
        {
            $url .= $nameFunction."/";
        }              
        return $url;   
    }   

    public function name($nameController, $type = 0)
    {
        $remove[] = "Controllers\\";
        if($type > 0)
        {
            $remove[] = "Controller";
        }        
        $nameController = str_replace($remove, "", $nameController); 
        return $nameController;   
    }
}