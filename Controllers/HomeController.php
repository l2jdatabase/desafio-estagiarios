<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

namespace Controllers;

use Apps\ControllerApp;
use Apps\GithubApp;
use Core\Controller;

class HomeController extends Controller
{
    private $ControllerApp;
    private $DataInfos;
    private $GithubApp;

    public function __construct()
    {
        //-----------------------------------------------------------
        // Instancias da Classe
        //-----------------------------------------------------------
        $this->GithubApp        = new GithubApp();
        $this->ControllerApp    = new ControllerApp();
        $this->DataInfos        = array(
            "nameController"  => $this->ControllerApp->name(__CLASS__),
        );
    }

    public function index()
    {
        $this->DataInfos["nameFunction"]    = __FUNCTION__;

        $this->loadTemplate('home', $this->DataInfos);
    }

    public function search($string = null)
    {
        $this->DataInfos["nameFunction"]    = __FUNCTION__;

        $userInfos  = null;
        $totalCount = 0;
        $search     = filter_input(INPUT_POST, "search", FILTER_SANITIZE_URL);
        if (!empty($search))
        {
            $userInfos  = $this->GithubApp->getUsers($search);
            $totalCount = $userInfos["total_count"];
        }

        $this->DataInfos["search"]      = $search;
        $this->DataInfos["usersInfos"]  = $userInfos;
        $this->DataInfos["totalCount"]  = array(count($userInfos["items"]), $totalCount);

        $this->loadTemplate('users/users-results', $this->DataInfos);
    }

    public function userInfo($login = null)
    {
        $data = $this->getUserInfo($login);

        echo
        '
            <div class="container-fluid">
                        <div class="row modal-user">
                            <div class="col-md-4">
                                <img src="' . $data["avatar_url"] . '" class="card-img-top" alt="...">
                            </div>
                            <div class="col-md-8">
                                <h4 class="title">' . $data["name"] . '</h4>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Username:
                                            <span class="left">' . $data["login"] . '</span>
                                        </p>

                                        <p>Cadastrado(a):
                                            <span class="left">' . date("d/m/Y", strtotime($data["created_at"])) . '</span>
                                        </p>
                                    </div>

                                    <div class="col-md-6 right">
                                        <p>Seguindo:
                                            <span class="right">' . $data["following"] . '</span>
                                        </p>

                                        <p>Seguidores:
                                            <span class="right">' . $data["followers"] . '</span>
                                        </p>
                                    </div>

                                    <div class="col-md-12">
                                        <p>URL:
                                            <span class="left">' . $data["html_url"] . '</span>
                                        </p>
                                    </div>

                                    <div class="col-md-12 right modal-user-btn">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">FECHAR</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
        ';
    }

    private function getUserInfo($login)
    {
        $login     = filter_var($login, FILTER_SANITIZE_EMAIL);
        return $this->GithubApp->getUser($login);;
    }
}
