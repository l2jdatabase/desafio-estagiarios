<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

namespace Controllers;

use Apps\ControllerApp;
use Core\Controller;
use Database\UsersDB;

class NotfoundController extends Controller
{
    private $ControllerApp;
    private $DataInfos;
    private $UsersDB;

    public function __construct()
    {
        //-----------------------------------------------------------
        // Instancias da Classe
        //-----------------------------------------------------------
        $this->ControllerApp     = new ControllerApp();
    }

    public function index()
    {
        $this->DataInfos["nameFunction"]    = __FUNCTION__;
        $this->DataInfos["nameController"]  = $this->ControllerApp->name(__CLASS__);
        //-----------------------------------------------------------
        // Autenticacao de usuarios
        //-----------------------------------------------------------
        $this->loadTemplate('404', $this->DataInfos);
    }
}
