<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

namespace Controllers;

use Core\Controller;

class LangController extends Controller
{
    public function index($lang)
    {
	   $_SESSION['lang'] = $lang;
	   header("Location: ".BASE_URL);
    }    
}
