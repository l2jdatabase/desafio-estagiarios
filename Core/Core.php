<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

namespace Core;

use Apps\DebugApp;
use Core\Api;
use Core\Language;

class Core
{
	protected 	$db;
	
	public function run()
	{
		$url = "/".(isset($_GET["q"]) ? $_GET["q"] : "");
		$url = $url != "/" ? $url : null;

		if(!empty($url))
		{
			$url = explode("/", $url);
			array_shift($url);				
		}

		$values = null;	
		//----------------------------------------------------------
		// APIS
		//----------------------------------------------------------
		if(!empty($url[0]) && $url[0] == "api")
		{
			array_shift($url);			
			$url = $this->RouterApi(implode("/", $url));
			$url = explode("/", $url);

			$values = $this->getValues($url, "Api");

			if(!method_exists($values["newPrefix"], $values["FunctionName"]))
			{
				return $this->returnErrorJson(400);
			}
		}
		//----------------------------------------------------------
		// CONTROLLERS
		//----------------------------------------------------------
		else if (!empty($url[0]))
		{		
			$url = $this->RouterWeb(implode("/", $url));
			$url = explode("/", $url);

			$values = $this->getValues($url, "Controller");
		}
		if(isset($values))
		{
			$ClassName		= $values["ClassName"];
			$FunctionName	= $values["FunctionName"];
			$params			= $values["params"];
			$newClassName   = $values["newPrefix"];
		}
		else
		{
			$FunctionName	= 'index';
			$params			= array();
			$newClassName   = '\Controllers\\HomeController';
		}
		$c = new $newClassName();

		call_user_func_array(array($c, $FunctionName), $params);
	}
	
	private function getValues($url, $prefix)
	{
		$values = array();
		
		$values["ClassName"] = ((!empty($url[0])) ? ucfirst($url[0]) : "Notfound").$prefix;
		$values["FunctionName"]	= "index";
		$values["params"]		= array();	
		array_shift($url);

		$values["newPrefix"] = '\\'.$prefix.'s'.'\\'.$values["ClassName"];
		if((isset($url[0])) && method_exists($values["newPrefix"], $url[0]))
		{
			$values["FunctionName"] = $url[0];	
		}
		array_shift($url);

		if(count($url) >= 1)
		{
			$values["params"] = $url;
		}
		unset($url);
	
		if(!file_exists($prefix.'s/'.$values["ClassName"].'.php') || !class_exists($values["newPrefix"]))
		{
			$values["ClassName"] 	= "Notfound".$prefix;
			$values["newPrefix"] = '\\'.$prefix.'s'.'\\'.$values["ClassName"];
		}

		return $values;
	}

	private function RouterApi($url)
	{
		global $routesApi;
		return $this->checkRoutes($url, $routesApi, 0);
	}

	private function RouterWeb($url)
	{
		global $routesWeb;
		return $this->checkRoutes($url, $routesWeb, 1);
	}

	private function RouterAddBar(&$routes)
	{
		$limpVar = True;
		foreach($routes as $key => $value)
		{
			if($limpVar){unset($routes);$limpVar = False;}
			$routes[$key] = $value;
			$routes[$key."/"] = $value;
		}
		return $routes;
	}

	private function checkRoutes($url, $routes, $routeType, $addBar = True)
	{
		$data = null;
		if(!empty($url) && !empty($routes))
		{
			if($addBar)
			{
				$routes = $this->RouterAddBar($routes);
			}
			
			foreach($routes as $pt => $newurl)
			{
				// Identifica os argumentos e substitui por regex
				$pattern = preg_replace('(\{[a-z0-9]{1,}\})', '([a-z0-9-]{1,})', $pt);

				// Faz o match da URL
				if(preg_match('#^('.$pattern.')*$#i', $url, $matches) === 1)
				{
					array_shift($matches);
					array_shift($matches);

					// Pega todos os argumentos para associar
					$itens = array();
					if(preg_match_all('(\{[a-z0-9]{1,}\})', $pt, $m))
					{
						$itens = preg_replace('(\{|\})', '', $m[0]);
					}

					// Faz a associação
					$arg = array();
					foreach($matches as $key => $match)
					{
						$arg[$itens[$key]] = $match;
					}

					// Monta a nova url
					foreach($arg as $argkey => $argvalue)
					{
						$newurl = str_replace(':'.$argkey, $argvalue, $newurl);
					}

					$data = $newurl;				
					break;
				}
			}
		}
		
		$dataFirst = $data;		
		if(is_array($data))
		{
			$dataFirst	= array_shift($data);
			$dataSecond = array_shift($data);
		}
		if(empty($dataSecond))
		{
			$dataSecond = "POST";
		}

		if(($routeType == 0) && (empty($dataFirst)))
		{
			$this->returnErrorJson(400);			
		}
		else if(($routeType == 0) && (!empty($dataFirst)) && ($dataSecond != $this->getMethod()))
		{
			$this->returnErrorJson(405);			
		}
		else
		{
			return  $dataFirst;
		}
	}
	
	private function debug($value, $DEBUG_TYPE = 2)
    {
		$instance["DebugApp"] = new DebugApp();
		return $instance["DebugApp"]->show($value, $DEBUG_TYPE);
	}

	private function getLang($String, $echo = True, $varArray = null)
	{
		$intance["Language"] = new Language();
		return $intance["Language"]->get($String, $echo, $varArray);
	}
	
	private function returnErrorJson($id)
	{
		$instance["Api"] = new Api();
		return $instance["Api"]->returnJson($instance["Api"]->returnError($id, false));
	}

	private function getMethod()
	{
		$instance["Api"] = new Api();
		return $instance["Api"]->getMethod();
	}
}
