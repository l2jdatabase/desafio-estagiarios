<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

namespace Core;

use Apps\ApiApp;
use Apps\DebugApp;
use Core\Language;

class Applicator
{
	protected 	$db;
	
    public function debug($value, $DEBUG_TYPE = 2)
    {
		$instance["DebugApp"] = new DebugApp();
		return $instance["DebugApp"]->show($value, $DEBUG_TYPE);
	}

	public function getLang($String, $echo = True, $varArray = null)
	{
		$intance["Language"] = new Language();
		return $intance["Language"]->get($String, $echo, $varArray);
	}
	
	public function returnErrorJson($id)
	{
		$instance["Api"] = new Api();
		return $instance["Api"]->returnJson($instance["Api"]->returnError($id, false));
	}
}