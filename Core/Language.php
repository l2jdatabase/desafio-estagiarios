<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

 namespace Core;
 
class Language
{
	protected $db;
    private $lang;
    private $ini;

    public function __construct()
    {
	   $this->lang = "en_us";	
	   if((isset($_SESSION['language_website'])) && !empty($_SESSION['language_website']) && file_exists('Assets/Lang/'.$_SESSION['language_website'].'.ini'))
	   {
	       $this->lang = $_SESSION['language_website'];
	   }
	   
	   $this->ini = parse_ini_file('Assets/Lang/'.$this->lang.'.ini');
    }
    
    public function get($keyWord, $echo = True, $varArray = null)
    {	
	   if(isset($this->ini[$keyWord]))
	   {
	      	$keyWord = $this->ini[$keyWord];
	   }

	   if(!empty($varArray) && isset($varArray))
	   {
			foreach($varArray as $key => $value)
			{
				$keyWord = str_replace("{".$key."}", $value, $keyWord);
			}
	   }
	
	   if($echo)
	   {
			echo $keyWord;
	   }
	   else
	   {
			return $keyWord;	       
       }
	}
}
