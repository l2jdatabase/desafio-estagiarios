<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

namespace Core;

use Apps\ApiApp;
use Apps\DebugApp;
use Core\Language;

class Database
{
    protected $db;

    public function __construct()
    {
        global $db;

        $this->db[0] = $db[0];
    }

    /***************************************************************
    *
    * INSERT -> Inseri dados no banco
    * 
    * $table: nome da tabela
    * $array: dados a serem inseridos
    * $url: caso preenchido redireciona para o url especidifcado
    * $returnId: retorna o id da linha inserida
    * $connectId: id da conexão
    * 
    ***************************************************************/

    public function insert($table, $array, $url = null, $returnId = null, $connectId = 0)
    {
        $arrayKeys = array_keys($array);

        $sql = $this->db[$connectId]->prepare("INSERT INTO $table (" . implode(",", $arrayKeys) . ") VALUES (:" . implode(",:", $arrayKeys) . ")");
        foreach ($array as $key => $value)
        {
            $sql->bindValue(':' . $key, $value);
        }
        $sql->execute();

        if (!empty($returnId)){ return $this->db[$connectId]->lastInsertId();}
        if (!empty($url)){header("Location: " . $url);}
    }

    /***************************************************************
    *
    * UPDATE -> Atualiza dados no banco
    * 
    * $table: nome da tabela
    * $array: dados a serem atualizados
    * $condition: condição para atualizar os dados  
    * $url: caso preenchido redireciona para o url especidifcado
    * $connectId: id da conexão
    * 
    ***************************************************************/

    public function update($table, $array, $condition, $url = null, $connectId = 0)
    {
        $array_keys_vals = "";
        $cond_keys_vals = "";

        $sizeArray = 1;
        foreach ($array as $key => $value)
        {
            if (count($array) <= 0)
            {
                return;
            }
            if (count($array) > $sizeArray)
            {
                $array_keys_vals .= $key . " = :" . $key . ",";
            }
            else
            {
                $array_keys_vals .= $key . " = :" . $key;
            }
            $sizeArray++;
        }

        $sizeCond = 1;
        foreach ($condition as $key => $value)
        {
            if (count($condition) <= 0)
            {
                return;
            }
            if (count($condition) > $sizeCond)
            {
                $cond_keys_vals .= $key . " = :" . $key . " AND ";
            }
            else
            {
                $cond_keys_vals .= $key . " = :" . $key;
            }
            $sizeCond++;
        }

        $sql = $this->db[$connectId]->prepare("UPDATE $table SET $array_keys_vals WHERE $cond_keys_vals");
        foreach ($array as $key => $value)
        {
            $sql->bindValue(':' . $key, $value);
        }
        foreach ($condition as $key => $value)
        {
            $sql->bindValue(':' . $key, $value);
        }
        $sql->execute();

        if (!empty($url))
        {
            header("Location: " . $url);
        }
    }

    /***************************************************************
    *
    * DELETE -> Deleta dados no banco
    * 
    * $table: nome da tabela
    * $condition: condição para deletar os dados  
    * $url: caso preenchido redireciona para o url especidifcado
    * $connectId: id da conexão
    * 
    ***************************************************************/

    public function delete($table, $column, $value, $url = null, $connectId = 0)
    {
        $sql = $this->db[$connectId]->prepare("DELETE FROM $table WHERE $column = :value");
        $sql->bindValue(":value", $value);
        $sql->execute();

        if (!empty($url)){header("Location: " . $url);}
    }

    /***************************************************************
    *
    * SELECT ALL -> Seleciona vários dados no banco
    * 
    * $table: nome da tabela
    * $columns: colunas a serem selecionadas
    * $condition: condição para deletar os dados  
    * $order: ordernação ou complementação da busca
    * $debug: 1 -> Mostra linha de busca / 2 Mostra resultado da busca
    * $connectId: id da conexão
    *
    * @return: Retorna um Array
    * 
    ***************************************************************/

    public function select_all($table, $columns = null, $condition = null, $order = null, $debug = null, $connectId = 0)
    {
        return $this->select(1, $table, $columns, $condition, $order, $debug, $connectId);
    }

    /***************************************************************
    *
    * SELECT ONE -> Seleciona 1 dado no banco
    * 
    * $table: nome da tabela
    * $columns: colunas a serem selecionadas
    * $condition: condição para deletar os dados  
    * $order: ordernação ou complementação da busca
    * $debug: 1 -> Mostra linha de busca / 2 Mostra resultado da busca
    * $connectId: id da conexão
    *
    * @return: Retorna uma String
    * 
    ***************************************************************/

    public function select_one($table, $columns = null, $condition = null, $order = null, $debug = null, $connectId = 0)
    {
        return $this->select(2, $table, $columns, $condition, $order, $debug, $connectId);
    }

    /***************************************************************
    *
    * SELECT COUNT -> Conta o resultado e apresenta o total
    * 
    * $table: nome da tabela
    * $columns: colunas a serem selecionadas
    * $condition: condição para deletar os dados  
    * $order: ordernação ou complementação da busca
    * $debug: 1 -> Mostra linha de busca / 2 Mostra resultado da busca
    * $connectId: id da conexão
    *
    * @return: Retorna um Int
    * 
    ***************************************************************/

    public function select_count($table, $column = null, $condition = null, $order = null, $debug = null, $connectId = 0)
    {
        return $this->select(3, $table, "COUNT($column) as total", $condition, $order, $debug, $connectId);
    }

    /***************************************************************
    *
    * EXISTE ROW -> Verifica a requisição existe no banco
    * 
    * $table: nome da tabela
    * $columns: colunas a serem selecionadas
    * $condition: condição para deletar os dados  
    * $order: ordernação ou complementação da busca
    * $debug: 1 -> Mostra linha de busca / 2 Mostra resultado da busca
    * $connectId: id da conexão
    *
    * @return: Retorna True ou False
    * 
    ***************************************************************/

    public function exist_row($table, $condition = null, $order = null, $debug = null, $connectId = 0)
    {
        return $this->select(4, $table, null, $condition, $order, $debug, $connectId);
    }

    /***************************************************************
    *
    * SELECT -> Função responsavel por SELEC ALL, ONE e COUNT
    * Função privada
    * 
    * $table: nome da tabela    
    * $columns: colunas a serem selecionadas
    * $condition: condição para deletar os dados  
    * $order: ordernação ou complementação da busca
    * $debug: 1 -> Mostra linha de busca / 2 Mostra resultado da busca
    * $connectId: id da conexão
    * 
    * $type: 1 -> select_all / 2 -> select_one / 2 -> select_count
    * 4 -> exist_row
    *
    ***************************************************************/

    private function select($type, $table, $columns = null, $condition = null, $order = null, $debug = null, $connectId = 0)
    {
        $data = array();
        if (empty($columns)){$columns = "*";}

        $sql = "SELECT $columns FROM $table";

        if (!empty($condition))
        {
            $cond_keys_vals = $this->conditionKey($condition);
            $sql .= " WHERE " . $cond_keys_vals;
        }

        if (!empty($order)){ $sql .= " " . $order;}

        if ($debug == 1){print_r($sql);exit();}

        $sql = $this->db[$connectId]->prepare($sql);

        if (!empty($condition))
        {
            $bindValue = "";
            foreach ($condition as $key => $value)
            {
                if (is_array($value))
                {
                    $value[0] = strtoupper($value[0]);
                    if ($value[0] == "IN")
                    {
                        if ($debug == 2)
                        {
                            $bindValue .= '[Type "IN" not insert in bindValue!](' . $value[2] . ')';
                        }
                    }
                    else
                    {
                        $sql->bindValue(':' . $key, $value[2]);
                        if ($debug == 2)
                        {
                            $bindValue .= '(:' . $key . ' , ' . $value[2] . ')<br />';
                        }
                    }
                }
                else
                {
                    $sql->bindValue(':' . $key, $value);
                    if ($debug == 2)
                    {
                        $bindValue .= '(:' . $key . ' , ' . $value . ')<br />';
                    }
                }
            }
        }

        if ($debug == 2)
        {
            print_r($bindValue);
            exit();
        }
        $sql->execute();

        if ($sql->rowcount() >= 1 && $type == 1)
        {
            $data = $sql->fetchall(\PDO::FETCH_ASSOC);
        }
        elseif ($sql->rowcount() >= 1 && $type == 2)
        {
            $data = $sql->fetch(\PDO::FETCH_ASSOC);
        }
        elseif ($sql->rowcount() >= 1 && $type == 3)
        {
            $data = $sql->fetch(\PDO::FETCH_ASSOC);            
            $data = $data["total"];
        }
        elseif ($sql->rowcount() >= 1 && $type == 4)
        {
            $data = True;
        }
        else
        {
            $data = null;
        }
        return $data;
    }

    /***************************************************************
    * CONDITIONKEY -> Prepara o array de condições
    * Função privada
    * 
    * $condition: array com condições para utilizar as 
    * funções de ação
    * 
    ***************************************************************/

    private function conditionKey($condition)
    {
        $cond_keys_vals = "";

        $i = 1;
        foreach ($condition as $key => $value)
        {
            if (!is_array($value) && count($condition) >= ($i + 1))
            {
                $cond_keys_vals .= $key . " = :" . $key . " AND ";
            }
            else if (!is_array($value) && count($condition) <= $i)
            {
                $cond_keys_vals .= $key . " = :" . $key;
            }
            else if (is_array($value))
            {
                $value[0] = strtoupper($value[0]);

                if ($value[0] == "IN" && is_array($value[2]))
                {
                    $value[2] = implode(',', array_map('intval', $value[2]));
                }

                if (count($condition) >= ($i + 1) && $value[0] == "IN")
                {
                    $cond_keys_vals .= $key . " " . $value[0] . " (" . $value[2] . ") " . $value[1] . " ";
                }
                else if (count($condition) <= $i && $value[0] == "IN")
                {
                    $cond_keys_vals .= $key . " " . $value[0] . " (" . $value[2] . ")";
                }
                else if (count($condition) >= ($i + 1) && $value[0])
                {
                    $cond_keys_vals .= $key . " " . $value[0] . " :" . $key . " " . $value[1] . " ";
                }
                else if (count($condition) <= $i && $value[0])
                {
                    $cond_keys_vals .= $key . " " . $value[0] . " :" . $key;
                }
            }
            $i++;
        }
        return $cond_keys_vals;
    }

    public function debug($value, $DEBUG_TYPE = 2)
    {
		$instance["DebugApp"] = new DebugApp();
		return $instance["DebugApp"]->show($value, $DEBUG_TYPE);
	}

	public function getLang($String, $echo = True, $varArray = null)
	{
		$intance["Language"] = new Language();
		return $intance["Language"]->get($String, $echo, $varArray);
	}
	
	public function returnErrorJson($id)
	{
		$instance["Api"] = new Api();
		return $instance["Api"]->returnJson($instance["Api"]->returnError($id, false));
	}

    public function EXIST_TABLE($connectId = 0, $sendMSG = 0, $table = null)
    {
        $sql = $this->db[$connectId]->prepare("SELECT table_name FROM information_schema.tables WHERE table_schema = :database AND table_name = :tabela");
        $sql->bindValue(':database', dbName);
        $sql->bindValue(':tabela', $table);
        $sql->execute();

        if($sql->rowCount() > 0)
        {
            return ($sendMSG) ? "<font color='green'>True</font>" : True;
        }
        else
        {
            return ($sendMSG) ? "<font color='red'>False</font>" : False;
        }
    }

    /*public function checkMysql()
    {   
        if($this->exist(0,0,"cms_settings"))
        {
            return True;
        }
        else
        {
            return False;
        }   
    }*/

}
