<?php
/**
* #############################
*  ##  #########     ########  ## ########                               ###
*  ##  #######  ##### #######  ## ###   ####             ##              ###
*  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
*  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
*  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
*  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
*  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
*  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
*  #############################
*  
*  @author Desenvolvido por L2JDatabase
*  Site: www.l2jdatabase.com
*  Projeto privado pago
*  Desenvolvido para ajudar administradores de Lineage II
*  
*  Este arquivo faz parte do projeto L2JDatabase.
*  PHP versao 7.3 ou Superior
**/

class Mysql
{
    protected 	$db;
    
    public function connect($connectId, $dbName, $dbhost, $dbport, $dbuser, $dbpass, $utf8 = true)
    {
        global $db;  
        define("dbName", $dbName);      

        if($utf8)
        {
            $db["$connectId"] = new PDO
            (
                "mysql:dbname=".$dbName.";host=".$dbhost.";port=".$dbport,$dbuser, $dbpass,
                array
                (
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                )
            );
        }
        else
        {
            $db["$connectId"] = new PDO
            (
                "mysql:dbname=".$dbName.";host=".$dbhost.";port=".$dbport,$dbuser, $dbpass,
            );
        }
        $db["$connectId"]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}