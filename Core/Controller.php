<?php

/**
 * #############################
 *  ##  #########     ########  ## ########                               ###
 *  ##  #######  ##### #######  ## ###   ####             ##              ###
 *  ##  #######  #####  ######  ## ###     ###   #####    ####   #####    ########     #####     #####    #####
 *  ##  ########  ###  #######  ## ###      ## ########   ####  ########  ### #####  ########   ######  ########
 *  ##  ###########   ########  ## ###     ### ##     ##  ##   ###    ### ###     ## ##     ##  ####    ##  ####
 *  ##  ##########  ##########  ## ###     ##  ##     ##  ##   ##      ##  ##     ## ##     ##    ####  ## ##
 *  ###  ######   ###########  ### ### #####   ###  ####  ####  ###  ####  ###  ###  ###  ####  #   ### ###   ###
 *  ####     ##        ##     #### #######      ########   ####  ########   ######    ########  ######   #######
 *  #############################
 *  
 *  @author Desenvolvido por L2JDatabase
 *  Site: www.l2jdatabase.com
 *  Projeto privado pago
 *  Desenvolvido para ajudar administradores de Lineage II
 *  
 *  Este arquivo faz parte do projeto L2JDatabase.
 *  PHP versao 7.3 ou Superior
 **/

namespace Core;

use Apps\DebugApp;
use Core\Language;
use Database\UsersDB;

class Controller
{
	protected 	$db;

	public function __construct()
	{
	}

	public function loadView($viewName, $viewData = array())
	{
		$file = 'Themes/' . $this->getCRMInfos("theme", false) . '/' . $viewName . '.php';

		if (file_exists($file))
		{
			extract($viewData);
			include $file;
		}
		else
		{
			echo "loadView <br /><br />";
			echo "ATTENTION: File does not exist! Please create it. <br />";
			echo  "FILE VIEW: " . $viewName . ".php";
		}
	}

	public function loadTemplate($viewName, $viewData = array())
	{
		$file = 'Themes/' . $this->getCRMInfos("theme", false) . '/' . 'index.php';

		if (file_exists($file))
		{
			extract($viewData);
			include $file;
		}
		else
		{
			echo "loadTemplate <br /><br />";
			echo "ATTENTION: File does not exist! Please create it. <br />";
			echo  "FILE VIEW: index.php";
		}
	}

	public function loadViewInTemplate($viewName, $viewData)
	{
		$file = 'Themes/' . $this->getCRMInfos("theme", false) . '/' . $viewName . '.php';

		if (file_exists($file))
		{
			extract($viewData);
			include $file;
		}
		else
		{
			echo "loadViewInTemplate <br /><br />";
			echo "ATTENTION: File does not exist! Please create it.<br />";
			echo  "FILE VIEW: " . $viewName . ".php";
		}
	}

	public function debug($value, $DEBUG_TYPE = 2)
	{
		$instance["DebugApp"] = new DebugApp();
		return $instance["DebugApp"]->show($value, $DEBUG_TYPE);
	}

	public function getLang($String, $echo = True, $varArray = null)
	{
		$intance["Language"] = new Language();
		return $intance["Language"]->get($String, $echo, $varArray);
	}

	public function tokenGeneratorForForm()
	{
		unset($_SESSION['token_form']);
		if (empty($_SESSION["token_form"]))
		{
			$tokenName = time() . rand(0, 999) . random_bytes(255) . time();
			$token = password_hash($tokenName, PASSWORD_BCRYPT);
			$_SESSION["token_form"] = $token;

			return $token;
		}
	}

	public function tokenCheckerOnForm($token_form)
	{
		$status = False;
		if (isset($_SESSION["token_form"]) && !empty($_SESSION["token_form"]) && $_SESSION["token_form"] == $token_form)
		{
			$status = True;
		}
		unset($_SESSION['token_form']);
		return $status;
	}

	public function getCRMInfos($key = null, $echo = True)
	{
		$CRM = array();

		$CRM["theme"] 			= "default";
		$CRM["title"] 			= "THE GIT SEARCH";
		$CRM["description"] 	= "????";

		if (empty($key))
		{
			$data = $CRM;
		}
		else if ((!empty($key)) && (array_key_exists($key, $CRM)))
		{
			$data = $CRM[$key];
		}
		else
		{
			$data = null;
		}

		if ($echo)
		{
			echo $data;
		}
		else
		{
			return $data;
		}
	}
}
